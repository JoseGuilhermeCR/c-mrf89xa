# c-mrf89xa

Linux device driver for mrf89xa from microchip updated for Linux kernel 6.1. The original version can be found in
https://github.com/basiliscos/c-mrf89xa/blob/master/mrf89xa.c.

## Device Tree

The driver has been adapted in order to use the device tree. An overlay example used
in Raspberry PI 4 Model B can be found in `module/mrf89xa.dts`

The tree is definitely not the way I'd like to, but this is my first time coding for
the kernel and using device trees. The mrf89xa is specially awkward to work with in this
model because it has 2 chip select pins, one for data and one for configuration.

For now, the device tree must supply two nodes, one for each chip select. The compatible string
for the driver is `microchip,mrf89xa`. The configuration device should have the `mrf_con`
empty property and the data device should have the `mrf_data` empty property.
This will cause the driver's `probe()` to run twice and acquire both SPI devices. Apart from this,
the nodes should specify the reset pin by using `mrfreset-gpios` and the interrupt pins using
`mrfirq-gpios`.

Note that apart from the two different chip selects and `mrf_{con,data}` properties, the two nodes
are identical. I'd like to unify both into a single node. The only thing preventing me for now is
the different chip selects for configuration and data.

## Cross-Compilation and Installation

The compilation should be pretty similar for most cases. The installation is shown as done
in a Raspberry PI 4 Model B.

Cross-compile module:
```bash
pushd module

export KDIR=/home/djouze/linux
export ARCH=arm64
export CROSS_COMPILE=aarch64-linux-gnu-

make -j
popd
```

Install module:
```bash
pushd module

export KDIR=/home/djouze/linux
export ARCH=arm64
export CROSS_COMPILE=aarch64-linux-gnu-

$ sudo -E make -C $KDIR M=$(pwd) ARCH=$ARCH CROSS_COMPILE=$CROSS_COMPILE INSTALL_MOD_PATH=/run/media/djouze/rootfs modules_install

popd
```


Compile overlay:
```bash
pushd module

dtc -@ -I dts -O dtb -o mrf89xa.dtbo mrf89xa.dts

popd
```

Install overlay:
```bash
pushd module

cp mrf89xa.dtbo /run/media/djouze/bootfs/overlays

popd
```

## Usage

1. Check that the device is properly connected
    - MOSI
    - MISO
    - CLK
    - IRQ1
    - IRQ0 (not actually used)
    - RESET
    - CSCON
    - CSDATA

2. Load an overlay or add the nodes with the compatible string in the device tree

3. If the device has been properly setup
    - Logs will show up in dmesg
    - `/proc/mrf89xa` will be created
    - `/dev/mrf89xa` will be created

4. Open the device, optionally using O_NONBLOCK
    ```c
    int fd = open("/dev/mrf89xa", O_RDWR)
    ```

5. Reset the device, optionally indicating whether it should be configured for use (initialize registers)
     ```c
    const uint8_t should_init_registers = 1;
    ioctl(fd, MRF89XA_IOC_RESET, &should_init_registers);
    ```

6. Set the device address
    ```c
    const struct mrf89xa_address addr = {
		.network_id = 1234,
		.node_id = 1,
	};
    ioctl(fd, MRF89XA_IOC_SET_ADDR, &addr);
    ```

7. Set the device frequency
    ```c
    const uint32_t frequency = MRF89XA_FREQ_915_MHZ;
    ioctl(fd, MRF89XA_IOC_SET_FREQ, &frequency);
    ```

8. Optionally enable listening
    ```c
    ioctl(fd, MRF89XA_IOC_LISTEN);
    ```
9. Use `read()` and `write()`
    - `read()` takes an `mrf89xa_frame` as parameter. The `data` member points to a buffer of length
    specified by the `length` member. The function returns the amount of bytes read which is also stored in the `length` member. The `addr` member stores the address to which the message was sent,
    which in most cases will be mrf89xa's own address, but it can also be used to indicate whether
    this message was broadcasted or not (`addr` == 0 indicates a broadcast). Care must be taken
    to reset the `length` member between `read()` calls because it's value will be changed.

    - `write()` takes an `mrf89xa_frame` as parameter. The `data` member points to a buffer of length
    specified by the `length` member. The `addr` member stores the address of the node the data will
    be sent to. The function returns the amount of bytes sent.

### Examples

Example code can be found under `example/`

- mrf89xa_transmitter: Tests transmission by sending text
- mrf89xa_receiver: Tests receivement by receiving text
- mrf89xa_transmitter_receiver: Tests both transmission and receivement, one at a time.

##  Generate compile_commands.json for module

Use [Bear](https://github.com/rizsotto/Bear).
```bash
$ bear -- make -j
```

clangd will complain about some flags, so keep in mind any such flags must be placed in the `.clangd` file,
as seen in: https://github.com/clangd/clangd/issues/1653
```
CompileFlags:
    Remove: [-fconserve-stack, -fno-allow-store-data-races, -mabi=lp64]

```
