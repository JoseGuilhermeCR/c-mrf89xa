#include "mrf89xa_util.h"

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(int argc, const char **argv)
{
	static uint8_t read_buffer[MRF89XA_MAX_PAYLOAD];
	static uint8_t write_buffer[MRF89XA_MAX_PAYLOAD];

	if (argc < 5) {
		fprintf(stderr,
			"Usage: %s <network> <node_address> <target> <start_listening>\n",
			argv[0]);
		return -1;
	}

	const uint32_t network = strtoul(argv[1], NULL, 0);
	const uint8_t node = strtoul(argv[2], NULL, 0);
	const uint8_t target_node = strtoul(argv[3], NULL, 0);
	bool should_start_listening = strtoul(argv[4], NULL, 0) == 1;

	const int fd = open_mrf_dev(network, node, true, false);
	if (fd < 0) {
		fprintf(stderr, "Failed to open device: %i\n", fd);
		return fd;
	}

	struct mrf89xa_frame frame;

	puts("Starting main loop...");

	while (true) {
		if (should_start_listening) {
			should_start_listening = false;
			goto listen;
		}

		fputs("Type your message: ", stdout);
		fgets((char *)write_buffer, sizeof(write_buffer) - 1, stdin);

		frame.data = write_buffer;
		frame.length = strlen((char *)write_buffer);
		frame.addr = target_node;

		int ret = write(fd, &frame, sizeof(frame));
		if (ret <= 0) {
			perror("Failed to write");
			continue;
		}

listen:
		frame.data = read_buffer;
		frame.length = sizeof(read_buffer);

		ret = read(fd, &frame, sizeof(frame));
		if (ret <= 0) {
			goto listen;
		}

		printf("read_frame length: %u addr: %u ret: %u\n", frame.length,
		       frame.addr, ret);
		for (uint8_t i = 0; i < frame.length; ++i) {
			printf("%c", frame.data[i]);
		}
		puts("");

		usleep(1000 * 1000);
	}

	close(fd);

	return 0;
}
