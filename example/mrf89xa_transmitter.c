#include "mrf89xa_util.h"

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(int argc, const char **argv)
{
	static uint8_t write_buffer[MRF89XA_MAX_PAYLOAD];

	if (argc < 4) {
		fprintf(stderr,
			"Usage: %s <network> <node_address> <target_node_address>\n",
			argv[0]);
		return -1;
	}

	const uint32_t network = strtoul(argv[1], NULL, 0);
	const uint8_t node = strtoul(argv[2], NULL, 0);
	const uint8_t target_node = strtoul(argv[3], NULL, 0);

	const int fd = open_mrf_dev(network, node, false, true);
	if (fd < 0) {
		fprintf(stderr, "Failed to open device: %i\n", fd);
		return fd;
	}

	puts("Starting main loop...");

	while (true) {
		fputs("Type your message: ", stdout);
		fgets((char *)write_buffer, sizeof(write_buffer) - 1, stdin);

		struct mrf89xa_frame write_frame = {
			.data = write_buffer,
			.length = strlen((char *)write_buffer),
			.addr = target_node,
		};

		int ret = write(fd, &write_frame, sizeof(write_frame));
		if (ret <= 0) {
			perror("Failed to write");
		}
	}

	close(fd);
	return 0;
}
