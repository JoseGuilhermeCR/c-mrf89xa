#ifndef MRF89XA_UTIL_H_
#define MRF89XA_UTIL_H_

#include <stdint.h>
#include <stdbool.h>

#include "../module/mrf89xa.h"

#if defined(__cplusplus)
extern "C" {
#endif

int open_mrf_dev(uint32_t network, uint8_t node, bool should_listen,
		 bool is_non_blocking);

#if defined(__cplusplus)
}
#endif

#endif
