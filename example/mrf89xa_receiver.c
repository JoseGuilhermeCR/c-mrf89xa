#include "mrf89xa_util.h"

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, const char **argv)
{
	static uint8_t read_buffer[MRF89XA_MAX_PAYLOAD];

	if (argc < 3) {
		fprintf(stderr, "Usage: %s <network> <node_address>\n",
			argv[0]);
		return -1;
	}

	const uint32_t network = strtoul(argv[1], NULL, 0);
	const uint8_t node = strtoul(argv[2], NULL, 0);

	const int fd = open_mrf_dev(network, node, true, true);
	if (fd < 0) {
		fprintf(stderr, "Failed to open device: %i\n", fd);
		return fd;
	}

	puts("Starting main loop...");

	while (true) {
		usleep(1000 * 1000);

		struct mrf89xa_frame read_frame = {
			.data = read_buffer,
			.length = sizeof(read_buffer),
		};

		int ret = read(fd, &read_frame, sizeof(read_frame));
		if (ret <= 0) {
			continue;
		}

		printf("read_frame length: %u addr: %u ret: %u\n",
		       read_frame.length, read_frame.addr, ret);
		for (uint8_t i = 0; i < read_frame.length; ++i) {
			printf("%c", read_frame.data[i]);
		}
		puts("");
	}

	close(fd);
	return 0;
}
