#include "mrf89xa_util.h"

#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#define DEV_PATH_NAME "/dev/mrf89xa"

int open_mrf_dev(uint32_t network, uint8_t node, bool should_listen,
		 bool is_non_blocking)
{
	int ret = 0;
	int fd = -1;

	const bool reset_should_init_registers = true;

	const struct mrf89xa_address addr = {
		.network_id = network,
		.node_id = node,
	};

	const uint32_t frequency = MRF89XA_FREQ_915_MHZ;

	ret = open(DEV_PATH_NAME, O_RDWR | (is_non_blocking ? O_NONBLOCK : 0));
	if (ret < 0) {
		goto finish;
	}

	fd = ret;

	ret = ioctl(fd, MRF89XA_IOC_RESET, &reset_should_init_registers);
	if (ret < 0) {
		goto finish;
	}

	ret = ioctl(fd, MRF89XA_IOC_SET_ADDR, &addr);
	if (ret < 0) {
		goto finish;
	}

	ret = ioctl(fd, MRF89XA_IOC_SET_FREQ, &frequency);
	if (ret < 0) {
		goto finish;
	}

	if (should_listen) {
		ret = ioctl(fd, MRF89XA_IOC_LISTEN);
		if (ret < 0) {
			goto finish;
		}
	}

finish:
	if (ret < 0) {
		if (fd >= 0) {
			close(fd);
		}

		return ret;
	}

	return fd;
}
