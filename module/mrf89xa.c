/* SPDX-License-Identifier: GPL-2.0 */
/*
 * Microchip MRF89XA SPI Driver
 *
 *  Copyright (C) 2015 Ivan Baidakou
 *  Copyright (C) 2023 José Guilherme <jose.guilherme.cr.bh@proton.me>
 */

#include <linux/delay.h>
#include <linux/device/class.h>
#include <linux/err.h>
#include <linux/gpio/consumer.h>
#include <linux/interrupt.h>
#include <linux/workqueue.h>
#include <linux/seq_file.h>
#include <linux/cdev.h>
#include <linux/of_device.h>
#include <linux/spi/spi.h>
#include <linux/list.h>
#include <linux/proc_fs.h>
#include <linux/workqueue.h>

#include "mrf89xa.h"

#define MRF89XA_DRV_NAME "mrf89xa"
#define MRF89XA_DRV_VERSION "1.0"

#define TX_WORKQUEUE_NAME "mrf89xa-tx-wq"

#define CMD_READ_REGISTER(N) (0x40 | (N << 1))
#define CMD_WRITE_REGISTER(N) (N << 1)

#define PAYLOAD_64 64

#define MRF89XA_FREQBAND_915 0x08

#define MRF89XA_TXPOWER_MINUS_8 ((0b111) << 1)
#define MRF89XA_TXPOWER_MINUS_5 ((0b110) << 1)
#define MRF89XA_TXPOWER_MINUS_2 ((0b101) << 1)
#define MRF89XA_TXPOWER_PLUS_1 ((0b100) << 1)
#define MRF89XA_TXPOWER_PLUS_4 ((0b011) << 1)
#define MRF89XA_TXPOWER_PLUS_7 ((0b010) << 1)
#define MRF89XA_TXPOWER_PLUS_10 ((0b001) << 1)
#define MRF89XA_TXPOWER_PLUS_13 (0)

#define CHIPMODE_STBYMODE ((0b001) << 5)

#define VCO_TRIM_11 0x06

#define FIFOSIZE_64 0xC0

#define CHIPMODE_SLEEPMODE (0)
#define CHIPMODE_FSMODE ((0b010) << 5)
#define CHIPMODE_RX ((0b011) << 5)
#define CHIPMODE_TX ((0b100) << 5)
#define CHIPMODE_MASK ((0b111) << 5)

#define MODSEL_FSK 0x80

#define DATAMODE_PACKET 0x04

#define IFGAIN_0 0x00

#define FREGDEV_80 0x04

#define BITRATE_25 0x07

#define IRQ0_RX_STDBY_SYNCADRS 0xC0
#define IRQ1_FIFO_OVERRUN_CLEAR 0b1
#define IRQ1_RX_STDBY_CRCOK 0x00
#define IRQ1_TX_TXDONE 0x08

#define IRQ1_PLL_LOCK_PIN_ON 0x01
#define IRQ1_PLL_LOCK (0b1 << 1)

#define IRQ0_TX_START_FIFONOTEMPTY 0x10

#define DEF_IRQPARAM1 0x08

#define PASSIVEFILT_378 0xA0
#define RXFC_FOPLUS100 0x03

#define FO_100 0x30

#define SYNC_ON 0x20
#define SYNC_SIZE_32 0x18
#define SYNC_ERRORS_0 0x00

#define DEF_RXPARAM3 0x07

#define FC_400 0xF0

#define CLKOUT_OFF 0x00

#define MANCHESTER_OFF 0x00

#define PKT_FORMAT_VARIABLE 0x80

#define PREAMBLE_SIZE_4 0x60

#define WHITENING_OFF 0x00

#define CRC_ON 0x08

#define ADRSFILT_ME_AND_00_AND_FF 0x06

#define FIFO_AUTOCLR_ON 0x00
#define FIFO_STBY_ACCESS_WRITE 0x00
#define FIFO_STBY_ACCESS_READ ((0b1) << 6)
#define FIFO_STBY_ACCESS_MASK ((0b1) << 6)

#define REG_GCON 0x00
#define REG_R1C 0x06
#define REG_P1C 0x07
#define REG_S1C 0x08
#define REG_FTXRXI 0x0D
#define REG_FTPRI 0x0E
#define REG_SYNC_WORD_1 0x16
#define REG_SYNC_WORD_2 0x17
#define REG_SYNC_WORD_3 0x18
#define REG_SYNC_WORD_4 0x19
#define REG_TXCON 0x1A
#define REG_NADDS 0x1D
#define REG_FCRC 0x1F

#define MRF_IRQ_TX_TIMEOUT_MS 50

#define MRF_CRYSTALL_FREQ 12800000

#define MRF89XA_RESET_DELAY_MS 100

#define MRF89XA_STATE_DEVICE_FOUND 1
#define MRF89XA_STATE_DEVICE_OPENED (1 << 1)
#define MRF89XA_STATE_ADDRESS_ASSIGNED (1 << 2)
#define MRF89XA_STATE_FREQ_ASSIGNED (1 << 3)
#define MRF89XA_STATE_TRANSMITTING (1 << 4)
#define MRF89XA_STATE_LISTENING (1 << 5)

#define MRF89XA_MAX_TX_QUEUE 10
#define MRF89XA_MAX_RX_QUEUE 10

struct mrf89xa_dev {
	struct spi_device *con_spi;
	struct spi_device *data_spi;
	struct gpio_desc *reset_pin;
	struct gpio_desc *irq0_pin;
	struct gpio_desc *irq1_pin;

	int irq0;
	int irq1;

	struct cdev cdev;
	struct class *sysfs_class;
	struct device *sysfs_device;
	spinlock_t state_lock;
	u32 state;
	atomic_t device_busy;

	struct timer_list tx_timeout_timer;
	struct mutex driver_mutex;
	struct proc_dir_entry *proc_file;
	wait_queue_head_t device_wait_queue;

	spinlock_t tx_queue_lock;
	atomic_t tx_queue_size;
	struct list_head tx_queue;
	wait_queue_head_t tx_wait_queue;

	spinlock_t rx_queue_lock;
	atomic_t rx_queue_size;
	struct list_head rx_queue;
	wait_queue_head_t rx_wait_queue;

	struct workqueue_struct *tx_worker;
};

/*
 * The payload may have up to 64 bytes, but since variable packet length
 * is used the first byte is used by the length. Therefore, only 63 bytes
 * can actually be used by the payload itself.
 */
struct mrf89xa_payload {
	u8 addr;
	u8 length;
	u8 data[MRF89XA_MAX_PAYLOAD];
	struct list_head list_item;
};

static const u8 por_register_values[] = {
	0x28, 0x88, 0x03, 0x07, 0x0C, 0x0F,
};

static u8 default_register_values[] = {
	CHIPMODE_STBYMODE | MRF89XA_FREQBAND_915 | VCO_TRIM_11,
	MODSEL_FSK | DATAMODE_PACKET | IFGAIN_0,
	FREGDEV_80,
	BITRATE_25,
	0,
	FIFOSIZE_64,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	IRQ0_RX_STDBY_SYNCADRS | IRQ1_RX_STDBY_CRCOK | IRQ1_TX_TXDONE,
	DEF_IRQPARAM1 | IRQ0_TX_START_FIFONOTEMPTY | IRQ1_PLL_LOCK_PIN_ON,
	0,
	PASSIVEFILT_378 | RXFC_FOPLUS100,
	FO_100,
	SYNC_SIZE_32 | SYNC_ON | SYNC_ERRORS_0,
	DEF_RXPARAM3,
	0,
	0,
	0,
	0,
	0,
	0,
	FC_400 | MRF89XA_TXPOWER_PLUS_13,
	CLKOUT_OFF,
	MANCHESTER_OFF | PAYLOAD_64,
	0,
	PKT_FORMAT_VARIABLE | PREAMBLE_SIZE_4 | WHITENING_OFF | CRC_ON |
		ADRSFILT_ME_AND_00_AND_FF,
	FIFO_AUTOCLR_ON | FIFO_STBY_ACCESS_WRITE,
};

static struct mrf89xa_dev *mrf_dev;

static int read_register(u8 index, u8 *value)
{
	const ssize_t ret =
		spi_w8r8(mrf_dev->con_spi, CMD_READ_REGISTER(index));

	if (ret >= 0) {
		*value = (u8)ret;
		return 0;
	}

	return -EIO;
}

static int write_register(u8 index, u8 value)
{
	const u8 buff[] = { CMD_WRITE_REGISTER(index), value };
	struct spi_transfer t = {
		.tx_buf = &buff,
		.len = ARRAY_SIZE(buff),
	};

	return spi_sync_transfer(mrf_dev->con_spi, &t, 1);
}

static int set_chip_mode(u8 mode)
{
	int ret = 0;
	u8 value = 0;

	ret = spi_w8r8(mrf_dev->con_spi, CMD_READ_REGISTER(REG_GCON));
	if (ret < 0) {
		return ret;
	}

	value = ((u8)ret & (~CHIPMODE_MASK)) | mode;

	return write_register(REG_GCON, value);
}

/*
 * Writes length + address + data to FIFO. Every byte is written as a separate transfer,
 * because the chip select must be toggled between the writes.
 */
static int write_fifo(u8 address, u8 length, u8 *data)
{
	const u8 total_length = length + 1;
	struct spi_transfer t = {
		.tx_buf = &total_length,
		.len = 1,
	};
	int ret = 0;

	ret = spi_sync_transfer(mrf_dev->data_spi, &t, 1);
	if (ret < 0) {
		return ret;
	}

	t.tx_buf = &address;
	ret = spi_sync_transfer(mrf_dev->data_spi, &t, 1);
	if (ret < 0) {
		return ret;
	}

	for (u8 i = 0; i < length; ++i) {
		t.tx_buf = data + i;
		ret = spi_sync_transfer(mrf_dev->data_spi, &t, 1);
		if (ret < 0) {
			return ret;
		}
	}

	return ret;
}

static void _device_acquire(void)
{
	bool is_acquired = false;

	wait_event(mrf_dev->device_wait_queue,
		   atomic_read(&mrf_dev->device_busy) == 0);

	while (!is_acquired) {
		spin_lock(&mrf_dev->state_lock);

		is_acquired = atomic_read(&mrf_dev->device_busy) == 0;
		if (is_acquired) {
			atomic_inc(&mrf_dev->device_busy);
		}

		spin_unlock(&mrf_dev->state_lock);
	}
}

static void _device_release(void)
{
	atomic_dec(&mrf_dev->device_busy);
	wake_up(&mrf_dev->device_wait_queue);
}

static void rx_switch_work(struct work_struct *unused)
{
	_device_acquire();

	spin_lock(&mrf_dev->state_lock);
	mrf_dev->state &= ~MRF89XA_STATE_TRANSMITTING;
	spin_unlock(&mrf_dev->state_lock);

	set_chip_mode(CHIPMODE_STBYMODE);
	set_chip_mode(CHIPMODE_RX);

	_device_release();
}
DECLARE_WORK(rx_switcher, rx_switch_work);

/*
 * If this function runs, the device has already been acquired in transfer_data.
 */
static void tx_timeout(struct timer_list *tl)
{
	bool should_listen = false;

	spin_lock(&mrf_dev->state_lock);
	mrf_dev->state &= ~MRF89XA_STATE_TRANSMITTING;
	should_listen = (mrf_dev->state & MRF89XA_STATE_LISTENING) != 0;
	spin_unlock(&mrf_dev->state_lock);

	_device_release();

	if (!atomic_read(&mrf_dev->tx_queue_size) && should_listen) {
		queue_work(mrf_dev->tx_worker, &rx_switcher);
	}
}

/*
 * This function acquires the device and does the procedure to fill it's FIFO
 * with data. The device is acquired here, but released in either tx_timeout or irq1_handler.
 */
static int transfer_data(u8 address, u8 length, u8 *data)
{
	int ret = 0;
	u8 access = 0;

	_device_acquire();

	ret = read_register(REG_FCRC, &access);
	if (ret < 0) {
		return ret;
	}

	access &= ~FIFO_STBY_ACCESS_MASK;

	ret = set_chip_mode(CHIPMODE_STBYMODE);
	if (ret < 0) {
		return ret;
	}

	access |= FIFO_STBY_ACCESS_WRITE;

	ret = write_register(REG_FCRC, access);
	if (ret < 0) {
		return ret;
	}

	ret = write_register(REG_FTPRI, default_register_values[REG_FTPRI] |
						IRQ1_PLL_LOCK);
	if (ret < 0) {
		return ret;
	}

	ret = write_register(REG_FTXRXI, default_register_values[REG_FTXRXI] |
						 IRQ1_FIFO_OVERRUN_CLEAR);
	if (ret < 0) {
		return ret;
	}

	spin_lock(&mrf_dev->state_lock);
	mrf_dev->state |= MRF89XA_STATE_TRANSMITTING;
	spin_unlock(&mrf_dev->state_lock);

	ret = write_fifo(address, length, data);
	if (ret < 0) {
		return ret;
	}

	ret = set_chip_mode(CHIPMODE_TX);
	if (ret < 0) {
		return ret;
	}

	timer_setup(&mrf_dev->tx_timeout_timer, tx_timeout, 0);
	mrf_dev->tx_timeout_timer.expires =
		jiffies + MRF_IRQ_TX_TIMEOUT_MS * HZ / 1000;

	add_timer(&mrf_dev->tx_timeout_timer);
	return ret;
}

static void transfer_work(struct work_struct *unused)
{
	int ret = 0;
	struct mrf89xa_payload *payload = NULL;
	int queue_size = atomic_read(&mrf_dev->tx_queue_size);

	while (queue_size) {
		spin_lock(&mrf_dev->tx_queue_lock);

		payload = list_first_entry(&mrf_dev->tx_queue,
					   struct mrf89xa_payload, list_item);
		list_del(&payload->list_item);

		spin_unlock(&mrf_dev->tx_queue_lock);

		ret = transfer_data(payload->addr, payload->length,
				    payload->data);
		if (ret < 0) {
			pr_err(MRF89XA_DRV_NAME
			       ": error in data transfer: %i\n",
			       ret);
		}

		kfree(payload);

		queue_size = atomic_dec_return(&mrf_dev->tx_queue_size);

		wake_up(&mrf_dev->tx_wait_queue);
	}
}
DECLARE_WORK(tx_processor, transfer_work);

static ssize_t mrf89xa_write(struct file *filp, const char *buff, size_t length,
			     loff_t *offset)
{
	int ret = 0;
	int queue_size = 0;
	struct mrf89xa_payload *payload = NULL;
	struct mrf89xa_frame frame;

	mutex_lock(&mrf_dev->driver_mutex);

	if ((filp->f_flags & O_NONBLOCK) &&
	    atomic_read(&mrf_dev->tx_queue_size) >= MRF89XA_MAX_TX_QUEUE) {
		ret = -EAGAIN;
		goto finish;
	}

	if (length != sizeof(struct mrf89xa_frame)) {
		ret = -EINVAL;
		goto finish;
	}

	ret = copy_from_user(&frame, buff, sizeof(struct mrf89xa_frame));
	if (ret < 0) {
		goto finish;
	}

	if (!frame.data || !frame.length ||
	    frame.length > MRF89XA_MAX_PAYLOAD) {
		ret = -EINVAL;
		goto finish;
	}

	payload = kzalloc(sizeof(struct mrf89xa_payload), GFP_KERNEL);
	if (!payload) {
		ret = -ENOMEM;
		goto finish;
	}

	payload->addr = frame.addr;
	payload->length = frame.length;

	ret = copy_from_user(&payload->data, frame.data, frame.length);
	if (ret < 0) {
		goto finish;
	}

	if (!(filp->f_flags & O_NONBLOCK)) {
		wait_event(mrf_dev->tx_wait_queue,
			   atomic_read(&mrf_dev->tx_queue_size) <
				   MRF89XA_MAX_TX_QUEUE);
	}

	spin_lock(&mrf_dev->tx_queue_lock);
	list_add_tail(&payload->list_item, &mrf_dev->tx_queue);
	spin_unlock(&mrf_dev->tx_queue_lock);

	queue_size = atomic_inc_return(&mrf_dev->tx_queue_size);

	if (queue_size == 1) {
		queue_work(mrf_dev->tx_worker, &tx_processor);
	}

	ret = (int)payload->length;

finish:
	mutex_unlock(&mrf_dev->driver_mutex);

	if (ret < 0 && payload) {
		kfree(payload);
	}

	return ret;
}

static ssize_t mrf89xa_read(struct file *filp, char *buff, size_t length,
			    loff_t *offset)
{
	int ret = 0;
	int bytes_to_read = 0;
	struct mrf89xa_payload *payload = NULL;
	struct mrf89xa_frame frame;

	if (length != sizeof(struct mrf89xa_frame)) {
		return -EINVAL;
	}

	if ((filp->f_flags & O_NONBLOCK) &&
	    (atomic_read(&mrf_dev->rx_queue_size) == 0)) {
		return -EAGAIN;
	}

	ret = copy_from_user(&frame, buff, sizeof(struct mrf89xa_frame));
	if (ret < 0) {
		return ret;
	}

	if (!frame.data || !frame.length) {
		return -EINVAL;
	}

	mutex_lock(&mrf_dev->driver_mutex);

	ret = wait_event_interruptible(mrf_dev->rx_wait_queue,
				       atomic_read(&mrf_dev->rx_queue_size) >
					       0);
	if (ret < 0) {
		goto finish;
	}

	spin_lock(&mrf_dev->rx_queue_lock);
	payload = list_first_entry(&mrf_dev->rx_queue, struct mrf89xa_payload,
				   list_item);
	list_del(&payload->list_item);
	atomic_dec(&mrf_dev->rx_queue_size);
	spin_unlock(&mrf_dev->rx_queue_lock);

	bytes_to_read = min(payload->length, frame.length);

	frame.length = bytes_to_read;
	frame.addr = payload->addr;

	ret = copy_to_user(frame.data, payload->data, bytes_to_read);
	if (ret < 0) {
		goto finish;
	}

	ret = copy_to_user(buff, &frame, sizeof(struct mrf89xa_frame));
	if (ret < 0) {
		goto finish;
	}

	ret = bytes_to_read;

finish:
	if (payload) {
		kfree(payload);
	}

	mutex_unlock(&mrf_dev->driver_mutex);

	return ret;
}

/*
 * Reads data from FIFO. Every byte is read as a separate transfer,
 * because the chip select must be toggled between the reads.
 */
static int read_fifo(u8 *destination, u8 length)
{
	int ret = 0;

	for (u8 i = 0; i < length && ret == 0; ++i) {
		struct spi_transfer t = {
			.rx_buf = destination + i,
			.len = 1,
		};

		ret = spi_sync_transfer(mrf_dev->data_spi, &t, 1);
	}

	return ret;
}

/*
 * This function starts running after irq1_handler. At this point, the device
 * has already been acquired.
 */
static void receive_frame_work(struct work_struct *unused)
{
	struct mrf89xa_payload *frame = NULL;
	int ret = 0;
	int queue_size = 0;
	u8 length_address[2];
	u8 access = 0;

	ret = set_chip_mode(CHIPMODE_STBYMODE);
	if (ret < 0) {
		goto finish;
	}

	ret = read_register(REG_FCRC, &access);
	if (ret < 0) {
		goto finish;
	}

	access = (access & ~FIFO_STBY_ACCESS_MASK) | FIFO_STBY_ACCESS_READ;

	ret = write_register(REG_FCRC, access);
	if (ret < 0) {
		goto finish;
	}

	ret = read_fifo(length_address, sizeof(length_address));
	if (ret) {
		goto finish;
	}

	frame = kzalloc(sizeof(struct mrf89xa_payload), GFP_KERNEL);
	if (!frame) {
		goto finish;
	}

	frame->length = length_address[0] - 1;
	frame->addr = length_address[1];

	ret = read_fifo(frame->data, frame->length);
	if (ret < 0) {
		goto finish;
	}

	spin_lock(&mrf_dev->rx_queue_lock);

	list_add_tail(&frame->list_item, &mrf_dev->rx_queue);
	queue_size = atomic_inc_return(&mrf_dev->rx_queue_size);
	if (queue_size > MRF89XA_MAX_RX_QUEUE) {
		struct mrf89xa_payload *dropped_frame = list_first_entry(
			&mrf_dev->rx_queue, struct mrf89xa_payload, list_item);
		list_del(&dropped_frame->list_item);
		kfree(dropped_frame);
		queue_size = atomic_dec_return(&mrf_dev->rx_queue_size);
	}

	spin_unlock(&mrf_dev->rx_queue_lock);

	ret = set_chip_mode(CHIPMODE_RX);
	if (ret < 0) {
		goto finish;
	}

finish:
	_device_release();
	if (ret < 0 && frame) {
		kfree(frame);
	} else {
		wake_up(&mrf_dev->rx_wait_queue);
	}
}
DECLARE_WORK(frame_receiver, receive_frame_work);

static irqreturn_t irq0_handler(int irq, void *dev_id)
{
	(void)irq;
	(void)dev_id;

	return IRQ_HANDLED;
}

static irqreturn_t irq1_handler(int irq, void *dev_id)
{
	(void)irq;
	(void)dev_id;

	spin_lock(&mrf_dev->state_lock);

	if (mrf_dev->state & MRF89XA_STATE_TRANSMITTING) {
		bool should_listen;

		mrf_dev->state &= ~MRF89XA_STATE_TRANSMITTING;
		should_listen = (mrf_dev->state & MRF89XA_STATE_LISTENING) != 0;

		spin_unlock(&mrf_dev->state_lock);

		_device_release();

		if (!atomic_read(&mrf_dev->tx_queue_size) && should_listen) {
			queue_work(mrf_dev->tx_worker, &rx_switcher);
		}

		del_timer(&mrf_dev->tx_timeout_timer);
	} else if (mrf_dev->state & MRF89XA_STATE_LISTENING) {
		const bool is_acquired = atomic_read(&mrf_dev->device_busy) ==
					 0;
		if (is_acquired) {
			atomic_inc(&mrf_dev->device_busy);
		}

		spin_unlock(&mrf_dev->state_lock);

		if (is_acquired) {
			queue_work(mrf_dev->tx_worker, &frame_receiver);
		} else {
			pr_warn(MRF89XA_DRV_NAME
				": ignore incoming frame, tx in progress\n");
		}
	} else {
		spin_unlock(&mrf_dev->state_lock);
		pr_warn(MRF89XA_DRV_NAME
			": irq1_handler called with unexpectedly, ignoring ?\n");
	}

	return IRQ_HANDLED;
}

static int mrf89xa_release(struct inode *inode, struct file *filp)
{
	struct mrf89xa_payload *frame, *tmp_frame;

	mutex_lock(&mrf_dev->driver_mutex);

	wait_event(mrf_dev->tx_wait_queue,
		   atomic_read(&mrf_dev->tx_queue_size) == 0);

	wait_event(mrf_dev->device_wait_queue,
		   atomic_read(&mrf_dev->device_busy) == 0);

	_device_acquire();

	set_chip_mode(CHIPMODE_STBYMODE);

	free_irq(mrf_dev->irq0, NULL);
	free_irq(mrf_dev->irq1, NULL);

	_device_release();

	spin_lock(&mrf_dev->rx_queue_lock);
	list_for_each_entry_safe(frame, tmp_frame, &mrf_dev->rx_queue,
				 list_item) {
		list_del(&frame->list_item);
		kfree(frame);
	}
	spin_unlock(&mrf_dev->rx_queue_lock);

	spin_lock(&mrf_dev->state_lock);
	mrf_dev->state &= ~MRF89XA_STATE_DEVICE_OPENED;
	spin_unlock(&mrf_dev->state_lock);

	mutex_unlock(&mrf_dev->driver_mutex);
	return 0;
}

static int mrf89xa_open(struct inode *inode, struct file *filp)
{
	int ret = 0;
	u32 state = 0;

	mutex_lock(&mrf_dev->driver_mutex);

	spin_lock(&mrf_dev->state_lock);
	state = mrf_dev->state;
	spin_unlock(&mrf_dev->state_lock);

	if (state & MRF89XA_STATE_DEVICE_OPENED) {
		ret = -EBUSY;
		goto err_busy;
	}

	ret = request_irq(mrf_dev->irq0, irq0_handler, IRQF_TRIGGER_RISING,
			  MRF89XA_DRV_NAME, NULL);
	if (ret < 0) {
		goto err_req_irq0;
	}

	ret = request_irq(mrf_dev->irq1, irq1_handler, IRQF_TRIGGER_RISING,
			  MRF89XA_DRV_NAME, NULL);
	if (ret < 0) {
		goto err_req_irq1;
	}

	spin_lock(&mrf_dev->state_lock);
	mrf_dev->state |= MRF89XA_STATE_DEVICE_OPENED;
	spin_unlock(&mrf_dev->state_lock);

	mutex_unlock(&mrf_dev->driver_mutex);

	return 0;

err_req_irq1:
	free_irq(mrf_dev->irq0, NULL);

err_req_irq0:
err_busy:
	mutex_unlock(&mrf_dev->driver_mutex);

	return ret;
}

static int initialize_registers(void)
{
	int ret = 0;

	for (int i = 0; i < ARRAY_SIZE(default_register_values); i++) {
		u8 got = 0;

		ret = write_register((u8)i, default_register_values[i]);
		if (ret < 0) {
			break;
		}

		ret = read_register((u8)i, &got);
		if (ret < 0) {
			break;
		}

		if (got != default_register_values[i]) {
			ret = -EIO;
			break;
		}
	}

	return ret;
}

static void reset_device(void)
{
	gpiod_set_value(mrf_dev->reset_pin, 1);
	usleep_idle_range(10000, 20000);
	gpiod_set_value(mrf_dev->reset_pin, 0);
	usleep_idle_range(10000, 20000);
}

static int cmd_reset(bool should_initialize_registers)
{
	int ret = 0;
	u32 state = 0;

	_device_acquire();

	spin_lock(&mrf_dev->state_lock);
	state = mrf_dev->state;
	spin_unlock(&mrf_dev->state_lock);

	state &= ~MRF89XA_STATE_ADDRESS_ASSIGNED;
	state &= ~MRF89XA_STATE_FREQ_ASSIGNED;
	state &= ~MRF89XA_STATE_TRANSMITTING;
	state &= ~MRF89XA_STATE_LISTENING;

	spin_lock(&mrf_dev->state_lock);
	mrf_dev->state = state;
	spin_unlock(&mrf_dev->state_lock);

	disable_irq(mrf_dev->irq0);
	disable_irq(mrf_dev->irq1);

	reset_device();

	if (should_initialize_registers) {
		pr_info(MRF89XA_DRV_NAME
			": reset and initialize registers: %i\n",
			should_initialize_registers);
		ret = initialize_registers();
	} else {
		pr_info(MRF89XA_DRV_NAME ": reset\n");
	}

	enable_irq(mrf_dev->irq0);
	enable_irq(mrf_dev->irq1);

	_device_release();
	return ret;
}

static int cmd_set_address(struct mrf89xa_address *addr)
{
	int ret = 0;

	u8 node_id = addr->node_id;
	u32 network_id = cpu_to_be32(addr->network_id);
	u8 *network_parts = (u8 *)&network_id;

	if (node_id == MRF89XA_BROADCAST_NODEADDR) {
		return -EINVAL;
	}

	_device_acquire();

	ret = write_register(REG_NADDS, node_id);
	if (ret < 0) {
		goto finish;
	}

	ret = write_register(REG_SYNC_WORD_1, network_parts[0]);
	if (ret < 0) {
		goto finish;
	}

	ret = write_register(REG_SYNC_WORD_2, network_parts[1]);
	if (ret < 0) {
		goto finish;
	}

	ret = write_register(REG_SYNC_WORD_3, network_parts[2]);
	if (ret < 0) {
		goto finish;
	}

	ret = write_register(REG_SYNC_WORD_4, network_parts[3]);
	if (ret < 0) {
		goto finish;
	}

	spin_lock(&mrf_dev->state_lock);
	mrf_dev->state |= MRF89XA_STATE_ADDRESS_ASSIGNED;
	spin_unlock(&mrf_dev->state_lock);

	pr_info(MRF89XA_DRV_NAME ": set node: %02x address: %x\n",
		addr->node_id, addr->network_id);

finish:
	_device_release();
	return ret;
}

static int cmd_set_freq(uint32_t freq)
{
	int ret = 0;

	const uint8_t r1 = freq & 0xff;
	const uint8_t p1 = (freq >> 8) & 0xff;
	const uint8_t s1 = (freq >> 16) & 0xff;

	if (freq != MRF89XA_FREQ_915_MHZ) {
		ret = -EINVAL;
		goto finish;
	}

	ret = write_register(REG_R1C, r1);
	if (ret < 0) {
		goto finish;
	}

	ret = write_register(REG_P1C, p1);
	if (ret < 0) {
		goto finish;
	}

	ret = write_register(REG_S1C, s1);
	if (ret < 0) {
		goto finish;
	}

	spin_lock(&mrf_dev->state_lock);
	mrf_dev->state |= MRF89XA_STATE_FREQ_ASSIGNED;
	spin_unlock(&mrf_dev->state_lock);

	pr_info(MRF89XA_DRV_NAME ": set frequency\n");

finish:
	return ret;
}

static int cmd_listen(void)
{
	spin_lock(&mrf_dev->state_lock);
	mrf_dev->state |= MRF89XA_STATE_LISTENING;
	spin_unlock(&mrf_dev->state_lock);

	queue_work(mrf_dev->tx_worker, &rx_switcher);

	pr_info(MRF89XA_DRV_NAME ": started listening\n");

	return 0;
}

long mrf89xa_ioctl_unlocked(struct file *filp, unsigned int cmd,
			    unsigned long arg)
{
	struct mrf89xa_address address;
	u32 tmp = 0;
	int ret = 0;

	if (_IOC_TYPE(cmd) != MRF89XA_IOC_MAGIC) {
		return -ENOTTY;
	}

	if (_IOC_NR(cmd) > MRF89XA_IOC_MAXNR) {
		return -ENOTTY;
	}

	if ((_IOC_DIR(cmd) & _IOC_READ) || (_IOC_DIR(cmd) & _IOC_WRITE)) {
		if (!access_ok((void __user *)arg, _IOC_SIZE(cmd))) {
			return -EINVAL;
		}
	}

	mutex_lock(&mrf_dev->driver_mutex);

	switch (cmd) {
	case MRF89XA_IOC_RESET:
		ret = get_user(tmp, (u8 __user *)arg);

		if (ret < 0) {
			goto finish;
		}

		ret = cmd_reset((bool)tmp);
		break;
	case MRF89XA_IOC_SET_ADDR:
		ret = copy_from_user(&address, (void __user *)arg,
				     sizeof(struct mrf89xa_address));
		if (ret < 0) {
			goto finish;
		}

		ret = cmd_set_address(&address);
		break;
	case MRF89XA_IOC_SET_FREQ:
		ret = get_user(tmp, (u32 __user *)arg);

		if (ret < 0) {
			goto finish;
		}

		ret = cmd_set_freq(tmp);
		break;
	case MRF89XA_IOC_LISTEN:
		ret = cmd_listen();
		break;
	default:
		pr_warn(MRF89XA_DRV_NAME
			": invalid command received in ioctl call: %u\n",
			cmd);
		ret = -ENOTTY;
		break;
	};

finish:
	mutex_unlock(&mrf_dev->driver_mutex);

	return ret;
}

static const struct file_operations mrf89xa_fops = {
	.read = mrf89xa_read,
	.write = mrf89xa_write,
	.open = mrf89xa_open,
	.release = mrf89xa_release,
	.unlocked_ioctl = mrf89xa_ioctl_unlocked,
	.owner = THIS_MODULE,
};

static int mrf89xa_dump_stats(struct seq_file *m, void *v)
{
	u8 regs[ARRAY_SIZE(default_register_values)];
	u64 freq = 0;
	u32 network_id = 0;
	u8 mode_id = 0;
	const char *mode = NULL;

	(void)v;

	mutex_lock(&mrf_dev->driver_mutex);

	for (int i = 0; i < sizeof(regs); i++) {
		read_register(i, &regs[i]);
	}

	network_id =
		(regs[REG_SYNC_WORD_1] << 24) | (regs[REG_SYNC_WORD_2] << 16) |
		(regs[REG_SYNC_WORD_3] << 8) | (regs[REG_SYNC_WORD_4] << 0);

	mode_id = regs[REG_GCON] & CHIPMODE_MASK;

	switch (mode_id) {
	case CHIPMODE_TX:
		mode = "tx";
		break;
	case CHIPMODE_RX:
		mode = "rx";
		break;
	case CHIPMODE_FSMODE:
		mode = "fs";
		break;
	case CHIPMODE_STBYMODE:
		mode = "stand-by";
		break;
	case CHIPMODE_SLEEPMODE:
		mode = "sleep";
		break;
	}

	freq = ((MRF_CRYSTALL_FREQ) / ((regs[REG_R1C] + 1) * 8)) *
	       ((75 * (regs[REG_P1C] + 1) + regs[REG_S1C]) * 9);

	seq_printf(m, "mrf89xa dump\n");

	seq_printf(m, "mode: %s\n", mode);
	seq_printf(m, "node address: 0x%.2x, network: 0x%.8x\n",
		   regs[REG_NADDS], network_id);
	seq_printf(m, "frequency: %llu Hz\n", freq);
	seq_printf(m, "raw register values\n");

	for (int i = 0; i < 8; ++i) {
		for (int j = 0; j < 4; j++) {
			const int index = i * 4 + j;
			const u8 value = regs[index];
			seq_printf(m, "register %02d (0x%.2x) = 0x%.2x %s",
				   index + 1, index, value,
				   (j == 3) ? "\n" : "| ");
		}
	}

	seq_printf(m, "irq0 value: %i irq1 value: %i\n",
		   gpiod_get_value(mrf_dev->irq0_pin),
		   gpiod_get_value(mrf_dev->irq1_pin));

	mutex_unlock(&mrf_dev->driver_mutex);
	return 0;
}

static int mrf89xa_proc_open(struct inode *inode, struct file *file)
{
	return single_open(file, mrf89xa_dump_stats, NULL);
}

static struct proc_ops mrf89xa_proc_ops = {
	.proc_open = mrf89xa_proc_open,
	.proc_read = seq_read,
	.proc_lseek = seq_lseek,
	.proc_release = single_release,
};

static int configure_spi(void)
{
	int ret = 0;

	mrf_dev->con_spi->max_speed_hz = 1000000;
	mrf_dev->con_spi->bits_per_word = 8;
	ret = spi_setup(mrf_dev->con_spi);
	if (ret < 0) {
		goto err;
	}

	mrf_dev->data_spi->max_speed_hz = 1000000;
	mrf_dev->data_spi->bits_per_word = 8;
	ret = spi_setup(mrf_dev->data_spi);
	if (ret < 0) {
		goto err;
	}

	pr_info(MRF89XA_DRV_NAME ": configured con_spi and data_spi\n");

err:
	return ret;
}

static int acquire_pins(struct spi_device *spi)
{
	int ret = 0;

	mrf_dev->reset_pin = gpiod_get(&spi->dev, "mrfreset", GPIOD_OUT_HIGH);
	if (IS_ERR(mrf_dev->reset_pin)) {
		pr_err(MRF89XA_DRV_NAME ": Failed to acquire reset\n");
		ret = PTR_ERR(mrf_dev->reset_pin);
		mrf_dev->reset_pin = NULL;
		return ret;
	}

	mrf_dev->irq0_pin = gpiod_get_index(&spi->dev, "mrfirq", 0, GPIOD_ASIS);
	if (IS_ERR(mrf_dev->irq0_pin)) {
		pr_err(MRF89XA_DRV_NAME ": Failed to acquire irq0\n");
		ret = PTR_ERR(mrf_dev->irq0_pin);
		goto err_get_irq0_pin;
	}

	mrf_dev->irq1_pin = gpiod_get_index(&spi->dev, "mrfirq", 1, GPIOD_ASIS);
	if (IS_ERR(mrf_dev->irq1_pin)) {
		pr_err(MRF89XA_DRV_NAME ": Failed to acquire irq1\n");
		ret = PTR_ERR(mrf_dev->irq1_pin);
		goto err_get_irq1_pin;
	}

	pr_info(MRF89XA_DRV_NAME ": acquired pins\n");

	ret = gpiod_to_irq(mrf_dev->irq0_pin);
	if (ret < 0) {
		goto err_to_irq;
	}
	mrf_dev->irq0 = ret;

	ret = gpiod_to_irq(mrf_dev->irq1_pin);
	if (ret < 0) {
		goto err_to_irq;
	}
	mrf_dev->irq1 = ret;

	return 0;

err_to_irq:
	gpiod_put(mrf_dev->irq1_pin);

err_get_irq1_pin:
	mrf_dev->irq1_pin = NULL;
	gpiod_put(mrf_dev->irq0_pin);

err_get_irq0_pin:
	mrf_dev->irq0_pin = NULL;

	gpiod_put(mrf_dev->reset_pin);
	mrf_dev->reset_pin = NULL;

	return ret;
}

/*
 * MRF89XA exposes two chip selects. One is used for configuration and
 * the other for data. The kernel will create an spi_device for each chip
 * select and we must store them in mrf_dev in order to send configuration and data.
 *
 * For now, this probe function expects two devices to be present on the device tree, one
 * for each chip select. The function will then run twice and only perform real setup
 * once both spi devices have been acquired.
 *
 */
static int mrf89xa_probe(struct spi_device *spi)
{
	int ret = 0;
	bool was_mrf_found = true;

	if (of_get_property(spi->dev.of_node, "mrf_con", NULL)) {
		pr_info(MRF89XA_DRV_NAME ": mrf_con found\n");
		mrf_dev->con_spi = spi;
	} else if (of_get_property(spi->dev.of_node, "mrf_data", NULL)) {
		pr_info(MRF89XA_DRV_NAME ": mrf_data found\n");
		mrf_dev->data_spi = spi;
	} else {
		return -ENODEV;
	}

	if (!mrf_dev->con_spi || !mrf_dev->data_spi) {
		return 0;
	}

	ret = acquire_pins(spi);
	if (ret < 0) {
		goto out;
	}

	ret = configure_spi();
	if (ret < 0) {
		goto out;
	}

	reset_device();

	for (u8 i = 0; i < ARRAY_SIZE(por_register_values) && was_mrf_found;
	     ++i) {
		u8 got;
		const u8 expected = por_register_values[i];

		ret = read_register(i, &got);
		pr_info(MRF89XA_DRV_NAME
			": probing register 0x%02x. got %02x, expected %02x\n",
			i, got, expected);

		if (ret < 0 || expected != got) {
			was_mrf_found = false;
			ret = -ENODEV;
		}
	}

	if (was_mrf_found) {
		mrf_dev->state |= MRF89XA_STATE_DEVICE_FOUND;
	}

out:
	return ret;
}

static const struct of_device_id mrf89xa_dt_ids[] = {
	{ .compatible = "microchip," MRF89XA_DRV_NAME },
	{},
};
MODULE_DEVICE_TABLE(of, mrf89xa_dt_ids);

static struct spi_driver mrf89xa_spi_driver = {
	.driver = {
		.name =		MRF89XA_DRV_NAME,
		.owner =	THIS_MODULE,
		.of_match_table = mrf89xa_dt_ids,
	},
	.probe = mrf89xa_probe,
};

static int mrf89xa_setup_spi(void)
{
	int ret = 0;

	ret = spi_register_driver(&mrf89xa_spi_driver);
	if (ret < 0) {
		pr_info(MRF89XA_DRV_NAME ": spi_register_driver failed\n");
		return ret;
	}

	if (!(mrf_dev->state & MRF89XA_STATE_DEVICE_FOUND)) {
		spi_unregister_device(mrf_dev->con_spi);
		spi_unregister_device(mrf_dev->data_spi);
		spi_unregister_driver(&mrf89xa_spi_driver);
		ret = -ENODEV;

		pr_info(MRF89XA_DRV_NAME
			": device hasn't been probed successfully: %i\n",
			ret);
	}

	return ret;
}

static int mrf89xa_create_cdev(void)
{
	int ret = 0;
	dev_t device_id = 0;

	ret = alloc_chrdev_region(&device_id, 1, 1, MRF89XA_DRV_NAME);
	if (ret < 0) {
		pr_err(MRF89XA_DRV_NAME
		       ": failed to allocate character device region\n");
		return ret;
	}

	pr_info(MRF89XA_DRV_NAME ": allocated major device number: %i\n",
		MAJOR(device_id));

	cdev_init(&mrf_dev->cdev, &mrf89xa_fops);
	mrf_dev->cdev.owner = THIS_MODULE;

	ret = cdev_add(&mrf_dev->cdev, device_id, 1);
	if (ret < 0) {
		pr_err(MRF89XA_DRV_NAME ": could not add character device\n");
		goto err_cdev_add;
	}

	mrf_dev->sysfs_class = class_create(MRF89XA_DRV_NAME);
	if (IS_ERR(mrf_dev->sysfs_class)) {
		pr_err(MRF89XA_DRV_NAME
		       ": failed to create device class: %li\n",
		       PTR_ERR(mrf_dev->sysfs_class));
		goto err_class_create;
	}

	pr_info(MRF89XA_DRV_NAME ": class created\n");

	mrf_dev->sysfs_device = device_create(mrf_dev->sysfs_class, NULL,
					      mrf_dev->cdev.dev, NULL,
					      MRF89XA_DRV_NAME);
	if (IS_ERR(mrf_dev->sysfs_device)) {
		pr_err(MRF89XA_DRV_NAME
		       ": failed to create sysfs device: %li\n",
		       PTR_ERR(mrf_dev->sysfs_device));
		goto err_device_create;
	}

	pr_info(MRF89XA_DRV_NAME ": sysfs device created\n");
	return ret;

err_device_create:
	class_destroy(mrf_dev->sysfs_class);

err_class_create:
	cdev_del(&mrf_dev->cdev);

err_cdev_add:
	unregister_chrdev_region(device_id, 1);

	return ret;
}

static int mrf89xa_create_procfs_entry(void)
{
	int ret = 0;

	mrf_dev->proc_file =
		proc_create(MRF89XA_DRV_NAME, 0444, NULL, &mrf89xa_proc_ops);
	if (!mrf_dev->proc_file) {
		pr_info(MRF89XA_DRV_NAME ": could not initialize /proc/%s\n",
			MRF89XA_DRV_NAME);
		ret = -ENOMEM;
	}

	return ret;
}

static int mrf89xa_init_queues_and_worker(void)
{
	int ret = 0;

	INIT_LIST_HEAD(&mrf_dev->tx_queue);
	atomic_set(&mrf_dev->tx_queue_size, 0);
	atomic_set(&mrf_dev->device_busy, 0);
	spin_lock_init(&mrf_dev->tx_queue_lock);
	spin_lock_init(&mrf_dev->state_lock);

	INIT_LIST_HEAD(&mrf_dev->rx_queue);
	atomic_set(&mrf_dev->rx_queue_size, 0);
	spin_lock_init(&mrf_dev->rx_queue_lock);

	init_waitqueue_head(&mrf_dev->tx_wait_queue);
	init_waitqueue_head(&mrf_dev->rx_wait_queue);
	init_waitqueue_head(&mrf_dev->device_wait_queue);

	mutex_init(&mrf_dev->driver_mutex);

	mrf_dev->tx_worker = create_singlethread_workqueue(TX_WORKQUEUE_NAME);
	if (!mrf_dev->tx_worker) {
		pr_err(MRF89XA_DRV_NAME
		       ": could not create singlethread workqueue\n");
		ret = -ENOMEM;
	}

	return ret;
}

static void mrf89xa_deinit_queues_and_worker(void)
{
	destroy_workqueue(mrf_dev->tx_worker);
}

static void mrf89xa_destroy_spi(void)
{
	gpiod_put(mrf_dev->reset_pin);
	gpiod_put(mrf_dev->irq0_pin);
	gpiod_put(mrf_dev->irq1_pin);
	spi_unregister_device(mrf_dev->con_spi);
	spi_unregister_device(mrf_dev->data_spi);
	spi_unregister_driver(&mrf89xa_spi_driver);
}

static void mrf89xa_destroy_cdev(void)
{
	device_destroy(mrf_dev->sysfs_class, mrf_dev->sysfs_device->devt);
	class_destroy(mrf_dev->sysfs_class);
	cdev_del(&mrf_dev->cdev);
	unregister_chrdev_region(mrf_dev->cdev.dev, 1);
}

static void mrf89xa_destroy_procfs_entry(void)
{
	remove_proc_entry(MRF89XA_DRV_NAME, NULL);
}

static __init int mrf89xa_init(void)
{
	int ret = 0;

	mrf_dev = kzalloc(sizeof(*mrf_dev), GFP_KERNEL);
	if (!mrf_dev) {
		pr_err(MRF89XA_DRV_NAME ": could not allocate device memory\n");
		return -ENOMEM;
	}

	ret = mrf89xa_init_queues_and_worker();
	if (ret < 0) {
		pr_err(MRF89XA_DRV_NAME ": init_queues_and_worker() failed.\n");
		goto err_init_queues_and_worker;
	}

	ret = mrf89xa_setup_spi();
	if (ret < 0) {
		pr_err(MRF89XA_DRV_NAME ": setup_spi() failed: %i.\n", ret);
		goto err_setup_spi;
	}

	ret = mrf89xa_create_cdev();
	if (ret < 0) {
		pr_err(MRF89XA_DRV_NAME ": create_cdev() failed.\n");
		goto err_create_cdev;
	}

	ret = mrf89xa_create_procfs_entry();
	if (ret < 0) {
		pr_err(MRF89XA_DRV_NAME ": create_procfs_entry() failed.\n");
		goto err_create_procfs_entry;
	}

	pr_info(MRF89XA_DRV_NAME ": inited\n");
	return 0;

err_create_procfs_entry:
	mrf89xa_destroy_cdev();

err_create_cdev:
	mrf89xa_destroy_spi();

err_setup_spi:
	mrf89xa_deinit_queues_and_worker();

err_init_queues_and_worker:
	kfree(mrf_dev);
	mrf_dev = NULL;

	return ret;
}

static void __exit mrf89xa_exit(void)
{
	mrf89xa_destroy_cdev();
	mrf89xa_destroy_procfs_entry();

	mrf89xa_deinit_queues_and_worker();

	mrf89xa_destroy_spi();

	kfree(mrf_dev);
	mrf_dev = NULL;
}

module_init(mrf89xa_init);
module_exit(mrf89xa_exit);

MODULE_AUTHOR("Ivan Baidakou");
MODULE_AUTHOR("José Guilherme");
MODULE_DESCRIPTION("Microchip MRF89XA SPI Driver");
MODULE_LICENSE("GPL");
MODULE_VERSION(MRF89XA_DRV_VERSION);
