/* SPDX-License-Identifier: GPL-2.0 WITH Linux-syscall-note */
/*
 * Microchip MRF89XA SPI Driver
 *
 *  Copyright (C) 2015 Ivan Baidakou
 *  Copyright (C) 2023 José Guilherme <jose.guilherme.cr.bh@proton.me>
 */

#ifndef _MRF89XA_H
#define _MRF89XA_H

#include <linux/types.h>
#include <linux/ioctl.h>

#define MRF89XA_MAX_PAYLOAD 63

struct mrf89xa_address {
	__u32 network_id;
	__u8 node_id;
};

struct mrf89xa_frame {
	__u8 *data;
	__u8 length;
	__u8 addr;
};

#define MRF89XA_IOC_MAGIC 'n'

#define MRF89XA_IOC_RESET _IOW(MRF89XA_IOC_MAGIC, 0, __u8)
#define MRF89XA_IOC_SET_FREQ _IOW(MRF89XA_IOC_MAGIC, 1, __u32)
#define MRF89XA_IOC_SET_ADDR _IOW(MRF89XA_IOC_MAGIC, 2, struct mrf89xa_address)
#define MRF89XA_IOC_LISTEN _IO(MRF89XA_IOC_MAGIC, 3)

#define MRF89XA_IOC_MAXNR 3

#define MRF89XA_BROADCAST_NODEADDR 0x00

#define MRF89XA_FREQ_915_MHZ ((0x32 << 16) | (0x64 << 8) | (0x77 << 0))

#endif
